lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'livesoccer/version'

Gem::Specification.new do |spec|
  spec.name           = 'livesoccer'
  spec.version        =  Livesoccer::VERSION
  spec.date           = '2014-06-21'
  spec.summary        = 'Update Live Soccer'
  spec.description    = 'Show Live soccer dude'
  spec.authors        = ["azhar amir"]
  spec.email          = 'cak.azharr@gmail.com'
  spec.homepage       = 'http://azharamir.com'
  spec.license        = 'MIT'

  spec.files          = `git ls-files`.split($/)
  spec.require_paths  = ["lib"]
  spec.executables    = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files     = spec.files.grep(%r{^(test|spec|features)/})

  spec.add_dependency "mechanize", "~> 2.7"
  spec.add_dependency "nokogiri", "~> 1.6"
  spec.add_dependency "railties", "~> 4.0"
end
