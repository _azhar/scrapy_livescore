module Livesoccer
  class << self
    attr_reader :category, :category_child
    def get_score(*path)
      body_response   = Livesoccer.get(*path)
      scrape_page     = body_response.xpath("//div[@class='content']/div")
      temp_data       = []
      scrape_page.each do |page|
        data = {}
        if page.at_xpath("div[1]/div[2]")
          @date  = page.at_xpath("div[1]/div[2]").text.strip
          if page.at_xpath("div[1]/div[1]/strong")
            @category_child   = page.at_xpath("div[1]/div[1]").text.strip
            @category         = page.at_xpath("div[1]/div[1]/strong").text.strip
          end
        else
          time_match        = page.at_xpath("div[1]").text.strip
          time_match = "" if time_match.scan(/\d+/).empty?
          regex_get_number  = @date.scan(/\d+/)
          if regex_get_number.count > 2
            @date = @date.to_date
            date  = "#{@date.strftime('%Y')} #{@date} #{time_match}".to_time
          else
            date  = "#{Time.now.strftime('%Y')} #{@date} #{time_match}".to_time
          end
          year, month, day, hour, minute = date.strftime('%Y'), date.strftime('%m'), date.strftime('%d'), date.strftime('%H'), date.strftime('%M')
          utc_date = Time.utc( year, month, day, hour, minute)
          data[:date] = utc_date.localtime
          data[:category_parent]  = category
          data[:category_child]   = category_child
          data[:home]             = page.at_xpath("div[2]").try(:text).try(:strip)
          data[:away]             = page.at_xpath("div[4]").try(:text).try(:strip)
          data[:scors]            = page.at_xpath("div[3]").try(:text).try(:strip)
          temp_data << data unless data[:home].nil?
        end
      end
      temp_data
    end
  end
end
