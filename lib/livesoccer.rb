require 'livesoccer/wrapper'
require 'mechanize'
require 'active_record/railtie'

module Livesoccer
  class << self
    def responding?
      @agent.current_page.code == "200"
    end
    def down?
      !responding?
    end
    def url(*path)
      ['http://livescore.com',*path].join('/')
    end
    def get(*path)
      @agent = Mechanize.new
      @agent.user_agent_alias = 'Mac Safari'
      http_url = self.url(*path)
      response = @agent.get(http_url)
      if responding?
        body_wrapper = Nokogiri::HTML(response.body)
      else
        raise ArgumentError, error_message(url, path)
      end
    end

    private
    def error_message(url, path)
      "The Url #{url} from path #{path} not valid"
    end
  end
end
